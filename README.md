Git Tips N Tricks
===

## Comandos

1. git branch -a

> Muestra todas las ramas

2. git checkout another_branch_name

> Brinca a otra rama

3. git push --all

> Hace push a todas las ramas

## Clonar un repo entero para establecernos en un commite especifico y crear una rama apartir de este commite

1. git clone -n repo_name

> clonamos todo el repo incluidos commits de todas las branch

2. git checkout mi_rama

> cambiamos a la rama que contiene el commit PS: Este paso se puede omitir y pasar directo al paso 3

3. git checkout id_commit

> cambiamos al commite, es decir, movemos el HEADER al commit indicado

4. git checkout -b nueva_rama id_sha_commit

> Creamos una nueva rama a partir de este último commite

## Push desde branch

1. git push origin mi_rama

> Hacer un push desde una rama al servidor